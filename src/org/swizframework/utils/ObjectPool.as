package org.swizframework.utils {
public class ObjectPool {
    private var type:Class;
    private var pool:Array = [];
    private var counter:int = -1;

    public function ObjectPool(type:Class) {
        this.type = type;
    }

    public function getObject():* {
        if (counter > -1) {
            return pool[counter--];
        }
        pool.push(new type());
        return pool[pool.length - 1];
    }

    public function disposeObject(obj:*):void {
        pool[++counter] = obj;
    }
}
}
