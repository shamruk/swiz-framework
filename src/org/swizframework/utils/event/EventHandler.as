/*
 * Copyright 2010 Swiz Framework Contributors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.swizframework.utils.event {
import flash.events.Event;
import flash.system.ApplicationDomain;

import mx.rpc.AsyncToken;

import org.swizframework.metadata.EventHandlerMetadataTag;
import org.swizframework.reflection.MetadataHostMethod;
import org.swizframework.reflection.MethodParameter;
import org.swizframework.utils.Pools;
import org.swizframework.utils.async.AsyncTokenOperation;
import org.swizframework.utils.async.IAsynchronousEvent;
import org.swizframework.utils.async.IAsynchronousOperation;

/**
 * Represents a deferred request for mediation.
 */
public class EventHandler {
    // ========================================
    // protected properties
    // ========================================

    /**
     * Backing variable for <code>metadata</code> property.
     */
    protected var _metadataTag:EventHandlerMetadataTag;

    /**
     * Backing variable for <code>method</code> property.
     */
    protected var _method:Function;

    /**
     * Backing variable for <code>eventClass</code> property.
     */
    protected var _eventClass:Class;

    /**
     * Backing variable for <code>domain</code> property.
     */
    protected var _domain:ApplicationDomain;

    /**
     * Strongly typed reference to metadataTag.host
     */
    protected var hostMethod:MetadataHostMethod;

    // ========================================
    // public properties
    // ========================================

    /**
     * The corresponding [EventHandler] tag.
     */
    public function get metadataTag():EventHandlerMetadataTag {
        return _metadataTag;
    }

    /**
     * The function decorated with the [EventHandler] tag.
     */
    public function get method():Function {
        return _method;
    }

    /**
     * The Event class associated with the [EventHandler] tag's event type expression (if applicable).
     */
    public function get eventClass():Class {
        return _eventClass;
    }

    /**
     * The ApplicationDomain in which to operate.
     */
    public function get domain():ApplicationDomain {
        return _domain;
    }

    // ========================================
    // constructor
    // ========================================

    /**
     * Constructor
     */

    public function init(metadataTag:EventHandlerMetadataTag, method:Function, eventClass:Class, domain:ApplicationDomain):void {
        _metadataTag = metadataTag;
        _method = method;
        _eventClass = eventClass;
        hostMethod = MetadataHostMethod(metadataTag.host);

    }

    // ========================================
    // public methods
    // ========================================

    /**
     * HandleEvent
     *
     * @param event The Event to handle.
     */
    public function handleEvent(event:Event):void {
        // ignore if the event types do not match
        if (( eventClass != null ) && !( event is eventClass ))
            return;

        var result:* = null;

        if (metadataTag.properties != null) {
            var args:Array = getEventArgs(event, metadataTag.properties);
            result = method.apply(null, args);
            args.length = 0;
            Pools.arrays.disposeObject(args)
        }
        else if (hostMethod.requiredParameterCount <= 1) {
            if (hostMethod.parameterCount > 0 && event is getParameterType(0))
                result = method(event);
            else
                result = method.apply();
        }

        if (event is IAsynchronousEvent && IAsynchronousEvent(event).step != null) {
            if (result is IAsynchronousOperation)
                IAsynchronousEvent(event).step.addAsynchronousOperation(result as IAsynchronousOperation);
            else if (result is AsyncToken)
                IAsynchronousEvent(event).step.addAsynchronousOperation(new AsyncTokenOperation(result as AsyncToken));
        }

        if (metadataTag.stopPropagation)
            event.stopPropagation();

        if (metadataTag.stopImmediatePropagation)
            event.stopImmediatePropagation();
    }

    // ========================================
    // protected methods
    // ========================================

    /**
     * Get Event Arguments
     *
     * @param event
     * @param properties
     */
    protected function getEventArgs(event:Event, properties:Array):Array {
        var args:Array = Pools.arrays.getObject();

        for each(var property:String in properties) {
            if (property.indexOf(".") < 0) {
                args[ args.length ] = event[ property ];
            }
            else {
                var chain:Array = property.split(".");
                var o:Object = event;
                while (chain.length > 1)
                    o = o[ chain.shift() ];

                args[ args.length ] = o[ chain.shift() ];
            }
        }

        return args;
    }

    /**
     * Get Parameter Type
     *
     * @param parameterIndex The index of parameter of the event handler method.
     * @returns The type for the specified parameter.
     */
    protected function getParameterType(parameterIndex:int):Class {
        var parameters:Array = hostMethod.parameters;

        if (parameterIndex < parameters.length)
            return ( parameters[ parameterIndex ] as MethodParameter ).type;

        return null;
    }
}
}