package org.swizframework.utils {
import org.swizframework.utils.event.EventHandler;

public class Pools {
    public static const handlers = new ObjectPool(EventHandler);
    public static const arrays = new ObjectPool(Array);
}
}
