/*
 * Copyright 2010 Swiz Framework Contributors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.swizframework.reflection {
import flash.utils.Dictionary;

/**
 * Base implementation of the IMetadataTag interface.
 * Implements getters and setters, <code>hasArg</code> and <code>getArg</code>
 * methods. Also adds <code>defaultArgName</code> support and defines
 * <code>asString</code> method for reconstructing the tag as it
 * looks in the source code (mostly for debugging purposes).
 */
public class BaseMetadataTag implements IMetadataTag {
    // ========================================
    // protected properties
    // ========================================

    protected var implementations:Dictionary = new Dictionary();

    /**
     * Backing variable for <code>name</code> property.
     */
    protected var _name:String;

    /**
     * Backing variable for <code>args</code> property.
     */
    protected var _args:Dictionary;

    protected var _firstArg:MetadataArg;

    /**
     * Backing variable for <code>host</code> property.
     */
    protected var _host:IMetadataHost;

    /**
     * Backing variable for <code>defaultArgName</code> property.
     */
    protected var _defaultArgName:String;

    // ========================================
    // public properties
    // ========================================

    /**
     * @inheritDoc
     */
    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }

    public function get args():Dictionary {
        return _args;
    }

    public function set args(value:Dictionary):void {
        _args = value;
        for each(var arg:MetadataArg in args) {
            _firstArg = arg;
            break;
        }
    }

    public function get firstArg():MetadataArg {
        return _firstArg;
    }

    /**
     * @inheritDoc
     */
    public function get host():IMetadataHost {
        return _host;
    }

    public function set host(value:IMetadataHost):void {
        _host = value;
    }

    /**
     * Name that will be assumed/used when a default argument value is provided,
     * e.g. [Inject( "someModel" )]
     */
    public function get defaultArgName():String {
        return _defaultArgName;
    }

    public function set defaultArgName(value:String):void {
        _defaultArgName = value;
    }

    /**
     * String showing what this tag looks like in code. Useful for debugging and log messages.
     */
    public function get asTag():String {
        return toString();
    }

    // ========================================
    // constructor
    // ========================================

    /**
     * Constructor
     */
    public function BaseMetadataTag() {

    }

    // ========================================
    // public methods
    // ========================================

    private static const EMPTY_STRING:String = "";

    /**
     * @inheritDoc
     */
    public function hasArg(argName:String):Boolean {
        return args[argName] || (args[EMPTY_STRING] && argName == defaultArgName);
    }

    /**
     * @inheritDoc
     */
    public function getArg(argName:String):MetadataArg {
        if (args[argName]) {
            return args[argName];
        } else if (argName == defaultArgName && args[EMPTY_STRING]) {
            return args[EMPTY_STRING];
        }
        return null;
    }

    public function copyFrom(metadataTag:IMetadataTag):void {
        name = metadataTag.name;
        args = metadataTag.args;
        host = metadataTag.host;
    }

    /**
     * Utility method useful for development and debugging
     * that returns string showing what this tag looked like defined in code.
     *
     * @return String representation of this tag as it looks in code.
     */
    public function toString():String {
        var str:String = "[" + name;

        if (args) {
            str += "( ";
            for each (var arg:MetadataArg in args) {
                if (arg.key != "")
                    str += arg.key + "=";
                str += "\"" + arg.value + "\","
            }
            str += " )";
        }

        str += "]";
        return str;
    }

    public function getCopyOfType(type:Class):* {
        if (!implementations[type]) {
            var tag:IMetadataTag = new type();
            tag.copyFrom(this);
            implementations[type] = tag;
        }
        return implementations[type];
    }
}
}