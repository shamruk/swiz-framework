/*
 * Copyright 2010 Swiz Framework Contributors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.swizframework.processors {
import flash.events.EventDispatcher;

import org.swizframework.core.Bean;
import org.swizframework.core.IBeanFactory;
import org.swizframework.core.ISwiz;
import org.swizframework.reflection.IMetadataTag;

/**
 * Metadata Processor
 */
public class BaseMetadataProcessor extends EventDispatcher implements IMetadataProcessor {
    // ========================================
    // protected properties
    // ========================================

    protected var swiz:ISwiz;
    protected var beanFactory:IBeanFactory;

    protected var _metadataNames:Array;

    // ========================================
    // public properties
    // ========================================

    /**
     * @inheritDoc
     */
    public function get metadataNames():Array {
        return _metadataNames;
    }

    /**
     *
     */
    public function get priority():int {
        return ProcessorPriority.DEFAULT;
    }

    // ========================================
    // constructor
    // ========================================

    /**
     * Constructor
     */
    public function BaseMetadataProcessor(metadataNames:Array, metadataClass:Class = null) {
        super();

        this._metadataNames = metadataNames;
    }

    // ========================================
    // public methods
    // ========================================

    /**
     * @inheritDoc
     */
    public function init(swiz:ISwiz):void {
        this.swiz = swiz;
        this.beanFactory = swiz.beanFactory;
    }

    /**
     * @inheritDoc
     */
    public function setUpMetadataTags(bean:Bean):void {
        for each (var metadataName:String in metadataNames) {
            for each (var metadataTag:IMetadataTag in bean.typeDescriptor.getMetadataTagsByName(metadataName)) {
                setUpMetadataTag(metadataTag, bean);
            }
        }
    }

    public function setUpMetadataTag(metadataTag:IMetadataTag, bean:Bean):void {
        // empty, subclasses should override
    }

    /**
     * @inheritDoc
     */
    public function tearDownMetadataTags(bean:Bean):void {
        for each (var metadataName:String in metadataNames) {
            for each (var metadataTag:IMetadataTag in bean.typeDescriptor.getMetadataTagsByName(metadataName)) {
                tearDownMetadataTag(metadataTag, bean);
            }
        }
    }

    public function tearDownMetadataTag(metadataTag:IMetadataTag, bean:Bean):void {
        // empty, subclasses should override
    }
}
}